-- CA 1
--Question 1
-- takes two numbers, adds them together, then doubles
add_and_double::(Num a)=> a -> a -> a
add_and_double x y = (x + y)* 2


--Question 2
--infix operator, preforms the function as add_and_double
(+*) x y = add_and_double x y


--Question 3
-- solve_quadratic_equation (4,5,0)
--NOT my work, used for screen cast ONLY; taken from:
--http://progopedia.com/example/quadratic-equation/174/
solve_quadratic_equation :: (Floating t, Eq t) => (t, t, t) -> [t]
solve_quadratic_equation (0, _, _) = []
solve_quadratic_equation (a, b, c)
      | d == 0 = [root (+)]
      | otherwise = [root (+), root (-)]
  where d = b*b - 4*a*c
        root sign = sign (-b) (sqrt d) / (2*a)
        

--Question 4
--Counting from 1, user defines 1 to X amount. returns a list to X amount
first_n :: (Num a, Enum a) => Int -> [a]
first_n x = take x[1..]


--Question 5
--first_n_integers :: (Num a, Enum a) => Integer -> [a]
--first_n_integers x [1..] 
--    |take_integer x
--    where take_integer x = take x[1..]


--Question 6
--double_factorial :: (Integer a) => a -> b
-- the above function header WAS working before other code was added
double_factorial :: (Ord a, Num a) => a -> a
double_factorial 0 = error "Incorrect input"
double_factorial n  = factorial n
    where factorial n = if n > 2 then n * double_factorial(n-1) else n
--    where factorial n = foldl(*) 1[1..n]


--Question 7
    

--Question 8
-- check to see wether given number is prime, returns a True/False value
isPrime :: Integer -> Bool
isPrime 2 = True
isPrime n = if n `mod` 2 /= 0
    then True
    else False


--Question 9
--using an infinite list to calculate primes, take this first 5 as it's infinite
definePrimes :: [Integer]
definePrimes = take 5[x | x <- [1..], x `mod` 2 /= 0]

    --
--Question 10
--sumR'::(Int a)=>[a]->a
-- Sums a list of numbers recursively. user must enter list
--sumR' [1,2,4,6,2,48]
sumR' :: Num a => [a] -> a
sumR'[] = 0
sumR' (x:xs) = x + sumR'(xs)


--Question 11
    --adds sums by cycling through list
    --foldl works by cycling through the list starting from the Left.
    --(+) operator given, so it adds each element
    --sumFl' [1,2,4,6,2,48]
sumFl'::(Num a)=>[a]->a
sumFl' = foldl (+) 0

--Question 12
    -- multiplying a list with foldr
    -- multiplyF[1,2,4,6,2,48]
multiplyF::(Num a)=>[a]->a
multiplyF = foldr (*) 1
 
--Question 13


--Question 14
--dotProduct :: (int a) => a -> b -> c
--dotProduct x y = product x :xs product y :ys


--Question 15
-- checks if a number is even, by calculating whether there is a remainder when
-- divided by two
is_even :: Integral a => a -> Bool
is_even n = if n `mod` 2 == 0
    then True
    else False
--this alternate solution from John Loane
is_evenB:: Integral a => a -> Bool
is_evenB n
    | even n = True
    | odd n = False


--Question 16
--list comprehension to replace a character from a String with 'x'
-- cycle through String and compare that no elements match vowels
--unixname "HEllo WOrld, the house,A-a-E-e-I-i-O-o-U-u!!"
unixname:: String -> String
unixname xs = [x|x <- xs, not(x `elem` "AaEeIiOoUu") ]
--unixname xs = [if x `elem` "AaEeIiOoUu" then '' else x |x <- xs]
--unixname x
--    |x == a = 'X'
--    |x == e = 'X'
--    |x == I = 'X'
--    |x == O = 'X'
--    |x == U = 'X'
--    |otherwise x



--Question 17
--intersection::(Num a)=> [a] -> [b] -> [c]
--intersection (x:xs) (y:ys) = if x xs == y ys then y : zs `intersection` else intersection


--Question 18
--list comprehension to censor characters from a String
--check if string contains vowels and replaces them with an 'x'
-- censor "HEllo WOrld, the house, A-a-E-e-I-i-O-o-U-u!"
censor:: String -> String
censor "" = error "empty list"
censor xs = [if x `elem` "AaEeIiOoUu" then 'x' else x |x <- xs]



